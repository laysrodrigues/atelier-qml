/**
 * Copyright (C) 2019 Lays Rodrigues <lays.rodrigues@kde.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <AtCore/AtCore>
#include <AtCore/MachineInfo>
#include <AtCore/Temperature>
#include <QApplication>
#include <QCommandLineParser>
#include <QIcon>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include <KLocalizedContext>
#include <KLocalizedString>
#include "core/AtCoreInstanceManager.h"
#include "core/logger/LoggerModel.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    app.setWindowIcon(QIcon(":/icons/atelier.png"));

#ifndef MOBILE
    qmlRegisterType<AtCore>("org.kde.atcore", 1, 0, "AtCore");
    qmlRegisterType<Temperature>("org.kde.atcore", 1, 0, "Temperature");
    qmlRegisterSingletonType<MachineInfo>("org.kde.atcore", 1, 0, "MachineInfo", [](QQmlEngine *engine, QJSEngine *jsEngine) -> QObject * {
        return MachineInfo::instance()->qmlSingletonRegister(engine, jsEngine);
    });
    qmlRegisterType<AtCoreInstanceManager>("org.kde.atelier", 1, 0, "AtCoreInstanceManager");
    qmlRegisterType<LoggerModel>("org.kde.atelier", 1, 0, "Logger");
#endif
    QQmlApplicationEngine engine;
    engine.rootContext()->setContextObject(new KLocalizedContext(&engine));
    engine.load(QUrl(QStringLiteral("qrc:/qml/main.qml")));

    return app.exec();
}
