/**
 * Copyright (C) 2019 Lays Rodrigues <lays.rodrigues@kde.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "LoggerModel.h"
#include <QTime>
#include <QVariant>

LoggerModel::LoggerModel(QAbstractListModel *parent) : QAbstractListModel(parent)
{
}

LoggerModel::~LoggerModel()
{
}

int LoggerModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    return logger.count();
}

QVariant LoggerModel::data(const QModelIndex &index, int role) const
{
    if (!checkIndex(index))
        return QVariant();

    if (role == Qt::DisplayRole)
        return logger.at(index.row());

    return QVariant();
}

void LoggerModel::append(const QString &message)
{
    QString txt = QTime::currentTime().toString(QStringLiteral("[hh:mm:ss:zzz] "));
    beginInsertRows(QModelIndex(), logger.count(), logger.count());
    logger.append(txt + message);
    endInsertRows();
}