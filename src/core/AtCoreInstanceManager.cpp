/**
 * Copyright (C) 2019 Lays Rodrigues <lays.rodrigues@kde.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "AtCoreInstanceManager.h"
#include <QVariant>

AtCoreInstanceManager::AtCoreInstanceManager(QAbstractListModel *parent) : QAbstractListModel(parent){

}
AtCoreInstanceManager::~AtCoreInstanceManager(){

}

int AtCoreInstanceManager::rowCount(const QModelIndex &parent) const{
  if (parent.isValid())
        return 0;
  return atcore_instances.count();
}

QVariant AtCoreInstanceManager::data(const QModelIndex &index, int role) const{
  // if (role == Manager::AtCoreRole) {
  return QVariant::fromValue<AtCore *>(atcore_instances.at(index.row()));
  // }
  // return QVariant();
}

AtCore *AtCoreInstanceManager::newConnection(const QString &port, int baud, const QString &fwName, bool disableRO){

  auto core = new AtCore();
  if(core->newConnection(port, baud, fwName, disableRO)){
    beginInsertRows(QModelIndex(), atcore_instances.count(), atcore_instances.count() + 1);
    atcore_instances.append(core);
    endInsertRows();
    return core;
  }
  return nullptr;
}

bool AtCoreInstanceManager::removeRow(int row, const QModelIndex &parent){
  beginRemoveRows(parent, row, 0);
  auto *core = atcore_instances.takeAt(row);
  core->closeConnection();
  delete (core);
  endRemoveRows();
  return true;
}
