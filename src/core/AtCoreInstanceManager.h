/**
 * Copyright (C) 2019 Lays Rodrigues <lays.rodrigues@kde.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once
#include <AtCore>
#include <QList>
#include <QObject>
#include <QAbstractListModel>
#include <QString>
#include <Qt>

class AtCoreInstanceManager : public QAbstractListModel{

  Q_OBJECT

public:
  explicit AtCoreInstanceManager(QAbstractListModel *parent=nullptr);
  ~AtCoreInstanceManager();
  virtual int rowCount(const QModelIndex &parent) const override;
  virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
  // bool insertRow(int row, const QModelIndex &parent = QModelIndex());
  Q_INVOKABLE AtCore* newConnection(const QString &port, int baud, const QString &fwName, bool disableRO = false);
  bool removeRow(int row, const QModelIndex &parent = QModelIndex());
  // virtual Qt::ItemFlags flags(const QModelIndex &index) const override;
  // enum Manager{
  //   AtCoreRole = Qt::UserRole +1
  // };

private:
  QList<AtCore*> atcore_instances;
};
