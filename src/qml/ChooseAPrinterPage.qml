/**
 * Copyright (C) 2019 Lays Rodrigues <lays.rodrigues@kde.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import QtQuick.Controls 2.5 as QQC2
import QtQuick.Layouts 1.1
import org.kde.kirigami 2.5 as Kirigami

Kirigami.Page{
  ColumnLayout{
    anchors.fill: parent
    QQC2.Label {
      text: i18n("Choose a 3D Printer")
      Layout.fillWidth: true
      horizontalAlignment: Text.AlignHCenter
    }
    Component {
        id: delegateComponent
        Kirigami.BasicListItem {
            id: listItem
            contentItem: RowLayout {
              QQC2.Label {
                    Layout.fillWidth: true
                    text: model.name
                    color: listItem.checked || (listItem.pressed && !listItem.checked && !listItem.sectionDelegate) ? listItem.activeTextColor : listItem.textColor
                }
                QQC2.Button{
                  id: editProfileButton
                  icon.name: "document-edit"
                  flat: true
                }
                QQC2.Button{
                  icon.name: "edit-delete"
                  flat: true
                }
            }
        }
    }
    ListView {
        id: mainList
        Layout.fillWidth: true
        Layout.fillHeight: true
        model: ListModel {
            id: listModel
              ListElement{
                name: "Dummy Printer 01"
              }
              ListElement{
                name: "Dummy Printer 02"
              }
        }
        delegate: Kirigami.DelegateRecycler {
            width: parent ? parent.width : implicitWidth
            sourceComponent: delegateComponent
        }
    }
    RowLayout{
      width: parent.width
      QQC2.Button{
        Layout.fillWidth: true
        icon.name: "list-add"
        text: i18n("Add New Printer")
      }
      QQC2.Button{
        Layout.fillWidth: true
        icon.name: "network-server"
        text: i18n("Print with multiple Printers")
      }
    }
  }
}
