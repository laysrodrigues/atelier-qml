/**
 * Copyright (C) 2019 Lays Rodrigues <lays.rodrigues@kde.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import "components" as Components
import QtQuick 2.9
import QtQuick.Controls 2.5 as QQC2
import QtQuick.Layouts 1.1
import org.kde.atcore 1.0
import org.kde.kirigami 2.5 as Kirigami

Kirigami.Page{
  id: pageRoot
  property AtCore core
  property url fileName
  property bool startPrint
  title: i18n("Temperatures Menu")
  ColumnLayout{
    anchors.fill: parent
    spacing: 10
    RowLayout{
      QQC2.Label {
        text: i18n("Warming up: 100% complete")
        Layout.fillWidth: true
        horizontalAlignment: Text.AlignHCenter
      }
    }

    RowLayout{
      id: layout
      Layout.alignment: Qt.AlignHCenter
      spacing: 10
      ColumnLayout{
        Components.TemperatureDial{
          id: bedDial
          from: 0
          to: 200
          value: parseInt(core.temperature.bedTargetTemperature)
          currTemp: parseInt(core.temperature.bedTemperature)
          onMoved:{
            core.setBedTemp(bedDial.value)
          }
        }
        QQC2.Label{
          text:i18n("Bed")
          Layout.alignment: Qt.AlignHCenter
        }
      }
      ColumnLayout{
        Components.TemperatureDial{
          id: extruderDial
          from: 0
          to: 250
          value: parseInt(core.temperature.extruderTargetTemperature)
          currTemp: parseInt(core.temperature.extruderTemperature)
          onMoved:{
            core.setExtruderTemp(extruderDial.value)
          }
        }
        QQC2.Label{
          text:i18n("Extruder 01")
          Layout.alignment: Qt.AlignHCenter
        }
      }
    }
    Components.BaseFooter{
     core: pageRoot.core
     fileName: pageRoot.fileName 
    }
  }
  Component.onCompleted: {
    if(startPrint){
      var file = fileName.toString().replace(/^(file:\/{2})/,"");
      core.print(file)
    }
  }
}
