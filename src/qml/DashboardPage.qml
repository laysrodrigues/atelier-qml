/**
 * Copyright (C) 2019 Lays Rodrigues <lays.rodrigues@kde.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import "components" as Components
import QtQuick 2.9
import QtQuick.Controls 2.5 as QQC2
import QtQuick.Layouts 1.1
import org.kde.atcore 1.0
import org.kde.kirigami 2.5 as Kirigami

Kirigami.Page{
  id: pageRoot
  property AtCore core
  property url fileName
  property LoggerPage loggerPage

  title: i18n("Dashboard")

  ColumnLayout{
    anchors.fill: parent
    RowLayout{
      QQC2.Label {
        text: i18n("Printing ") + core.percentagePrinted.toString() + i18n(" % complete")
        Layout.fillWidth: true
        horizontalAlignment: Text.AlignHCenter
      }
    }
    GridLayout{
      columns: 3
      Layout.fillWidth: true
      Layout.fillHeight: true
      columnSpacing: 10
      rowSpacing: 10
      QQC2.Button{
        text: i18n("Temperatures");
        Layout.fillWidth: true
        Layout.fillHeight: true
        flat: true
        icon.width: 48
        icon.height: 48
        icon.name: "weather-clear"
        onClicked: pageStack.layers.push(Qt.resolvedUrl("TemperaturesPage.qml"), {core: core})
      }

      QQC2.Button{
        text: i18n("Temperatures Chart");
        Layout.fillWidth: true
        Layout.fillHeight: true
        flat: true
        icon.width: 48
        icon.height: 48
        icon.name: "weather-clear"
        onClicked: overlayChart.open()
      }

      QQC2.Button{
        text: i18n("Axis and Filament");
        Layout.fillWidth: true
        Layout.fillHeight: true
        flat: true
        icon.width: 48
        icon.height: 48
        icon.name: "applications-engineering"
        onClicked: pageStack.layers.push(Qt.resolvedUrl("AxisPage.qml"), {core: core})
      }

      QQC2.Button{
        text: "3DView";
        Layout.fillWidth: true
        Layout.fillHeight: true
        flat: true
        icon.width: 48
        icon.height: 48
        icon.name: "draw-cuboid"
        onClicked: overlay3D.open()
      }

      QQC2.Button{
        text: "Camera View";
        Layout.fillWidth: true
        Layout.fillHeight: true
        flat: true
        icon.width: 48
        icon.height: 48
        icon.name: "camera-video"
        onClicked: overlayCamera.open()
      }
      QQC2.Button{
        text: i18n("Log");
        Layout.fillWidth: true
        Layout.fillHeight: true
        flat: true
        icon.width: 48
        icon.height: 48
        icon.name: "accessories-text-editor"
        onClicked: pageStack.layers.push(loggerPage)
      }
    }
    Components.BaseFooter{
     core: pageRoot.core
     fileName: pageRoot.fileName
    }
  }
  Kirigami.OverlaySheet{
    id: overlay3D
    ColumnLayout{
      width: pageRoot.width * 0.7
      height: pageRoot.height * 0.7
      Rectangle{
        Layout.fillWidth: true
        Layout.fillHeight: true
      }
    }
  }
  Kirigami.OverlaySheet{
    id: overlayChart
    ColumnLayout{
      width: pageRoot.width * 0.7
      height: pageRoot.height * 0.7
      Rectangle{
        Layout.fillWidth: true
        Layout.fillHeight: true
      }
    }
  }
  Components.RemoteCamera{
    id: overlayCamera
    pwidth: pageRoot.width * 0.7
    pheight: pageRoot.height * 0.7
  }
}
