/* Atelier KDE Printer Host for 3D Printing
    Copyright (C) <2017-2019>
    Author: Patrick José Pereira - patrickelectric@gmail.com
    Author: Lays Rodrigues Silva - lays.rodrigues@kde.org

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; either version 3 of
    the License or any later version accepted by the membership of
    KDE e.V. (or its successor approved by the membership of KDE
    e.V.), which shall act as a proxy defined in Section 14 of
    version 3 of the license.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.7
import org.kde.kirigami 2.5 as Kirigami
import QtQuick.Controls 2.2
import org.kde.atcore 1.0
import org.kde.atelier 1.0

Kirigami.Page {
    id: loggerPage
    title: "Log"
    property AtCore core
    Logger{
        id: logger
    }
    ListView{
        anchors.fill: parent
        id: modelView
        verticalLayoutDirection: ListView.BottomToTop
        model: logger
        delegate: Text{
            text: model.display
            // color: "white"
        }
    }
    Connections{
        target: core
        onAtcoreMessage:{
            logger.append(msg)
        }
        onReceivedMessage:{
            logger.append(message)
        }
        onPushedCommand:{
            logger.append(comm)
        }
    }
}
