/**
 * Copyright (C) 2019 Lays Rodrigues <lays.rodrigues@kde.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import QtQuick.Dialogs 1.3
import QtQuick.Controls 2.5 as QQC2
import QtQuick.Layouts 1.1
import org.kde.kirigami 2.5 as Kirigami
import org.kde.atcore 1.0
import org.kde.atelier 1.0

Kirigami.Page{
  id: pageRoot
  property var currProfile
  property int baudRate
  property AtCore core
  property string firmware
  property int index
  property bool newConn
  property string profile
  property url selectedFile
  property string serialPort

  title: i18n("Printer: ") + currProfile + " @ " + core.connectedPort
  ColumnLayout{
    anchors.fill: parent
    RowLayout{
      Layout.alignment: Qt.AlignRight
      Layout.fillWidth: true
      QQC2.Button {
        text: i18n("Connected Printers")
        icon.name: "network-connect"
        onClicked: pageStack.layers.push(Qt.resolvedUrl("ChooseAPrinterPage.qml"))
      }
    }
    GridLayout{
      Layout.fillWidth: true
      Layout.fillHeight: true
      columns: 2

      QQC2.Button{
        Layout.fillWidth: true
        icon.name: "document-open"
        icon.width: 96
        icon.height: 96
        text: i18n("Print object from GCode file")
        onClicked: {
          dialog.open()
        }
      }
      QQC2.Button{
        Layout.fillWidth: true
        icon.name: "applications-utilities"
        icon.width: 96
        icon.height: 96
        text: i18n("Dashboard")
        onClicked: pageStack.layers.push(Qt.resolvedUrl("DashboardPage.qml"), {core: core, loggerPage: loggerPage})
      }
    }
    RowLayout{
      width: parent.width
      QQC2.Button{
        Layout.fillWidth: true
        icon.name: "media-eject"
        text: i18n("Disconnect")
        onClicked: {
          core.closeConnection()
          pageStack.replace(Qt.resolvedUrl("NewPrinterPage.qml"))
        }
      }
      QQC2.Button{
        Layout.fillWidth: true
        icon.name: "list-add"
        text: i18n("Add new printer")
        onClicked:{
          pageStack.layers.push(Qt.resolvedUrl("NewPrinterPage.qml"))
        }
      }
    }
  }
  AtCoreInstanceManager{
    id: manager
  }

  Component.onCompleted: {
    if(newConn){
      core = manager.newConnection(serialPort, baudRate, firmware)
      if(core)
        loggerPage.core = core
      else{
        pageStack.replace(Qt.resolvedUrl("NewPrinterPage.qml"))
        showPassiveNotification(i18n("Failed to connect! Check your device!"))
      }

    }
    else{
      // core = manager.data(index)
    }
  }

  LoggerPage{
    id: loggerPage
    visible: false
  }

  FileDialog{
    id: dialog
    title: i18n("Please, select a GCode")
    folder: shortcuts.home
    nameFilters: ["GCode Files (*.gcode *.gco)"]
    onAccepted:{
      selectedFile = dialog.fileUrl
      pageStack.layers.push(Qt.resolvedUrl("TemperaturesPage.qml"), {core: core, fileName: fileUrl, startPrint: true})
    }
    onRejected: {
      selectedFile = ""
    }
  }
}
