/**
 * Copyright (C) 2019 Lays Rodrigues <lays.rodrigues@kde.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import QtQuick.Controls 2.5 as QQC2
import QtQuick.Layouts 1.1
import org.kde.kirigami 2.5 as Kirigami

QQC2.Dial{
  id: control
  stepSize: 5
  property int currTemp
  background: Rectangle {
      id: back
      color: "transparent"
      radius: width / 2
      border.width: 8
      border.color: Kirigami.Theme.activeTextColor
      opacity: control.enabled ? 1 : 0.3
      ColumnLayout{
        anchors.centerIn: parent
        QQC2.Label{
          text: parseInt(currTemp)
        }
        QQC2.Label{
          text: parseInt(control.value) // Here is the target tmp
          opacity: 0.5
        }
      }
    }
    handle: Rectangle {
        id: handleItem
        x: control.background.x + control.background.width / 2 - width / 2
        y: control.background.y + control.background.height / 2 - height / 2
        width: 18
        height: 18
        color: Kirigami.Theme.textColor
        radius: 8
        antialiasing: true
        opacity: control.enabled ? 1 : 0.3
        transform: [
            Translate {
                y: -Math.min(control.background.width, control.background.height) * 0.53 + handleItem.height / 2
            },
            Rotation {
                angle: control.angle
                origin.x: handleItem.width / 2
                origin.y: handleItem.height / 2
            }
        ]
    }
    MouseArea {
        anchors.fill: parent
        propagateComposedEvents: true
        acceptedButtons: Qt.NoButton
        onPressed: mouse.accepted = false
        onWheel: {
          wheel.angleDelta.y > 0 ? increase() : decrease()
          changeDialColor()
          onMoved()
        }
    }

    onMoved: changeDialColor()

    function changeDialColor() {
        var value = control.value
        var per30 = 0.3*control.to
        var per60 = 0.6*control.to
        var per80 = 0.8*control.to
        if (value < per30){
          back.border.color = Kirigami.Theme.activeTextColor;
        }
        else if (value >= per30 && value <= per60){
          back.border.color = Kirigami.Theme.positiveTextColor;
        }
        else if (value > per60 && value <=per80){
          back.border.color = Kirigami.Theme.neutralTextColor;
        }
        else{
          back.border.color=Kirigami.Theme.negativeTextColor;
        }
    }
}
