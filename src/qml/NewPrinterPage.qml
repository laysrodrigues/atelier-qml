/**
 * Copyright (C) 2019 Lays Rodrigues <lays.rodrigues@kde.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import QtQuick.Controls 2.5 as QQC2
import QtQuick.Layouts 1.1
import org.kde.atcore 1.0
import org.kde.kirigami 2.5 as Kirigami

Kirigami.Page{
  id: pageRoot
  property alias currProfile: printerProfile.currentText

  AtCore{
    id: core
  }
  title: i18n("Welcome to Atelier!")
  ColumnLayout{
    anchors.fill: parent
    QQC2.Label {
        text: i18n("Connect to a 3D Printer")
        Layout.fillWidth: true
        horizontalAlignment: Text.AlignHCenter
    }
    GridLayout{
      Layout.fillWidth: true
      Layout.fillHeight: true
      Layout.alignment: Qt.AlignHCenter
      columns:2
      QQC2.Label {
        text: i18n("Printer Profile:")
        horizontalAlignment: Text.AlignLeft
      }
      RowLayout{
        QQC2.ComboBox{
          id: printerProfile
          model: MachineInfo.profileNames()
        }
        QQC2.Button{
          id: editProfile
          text: i18n("Edit")
          icon.name: "document-properties"
          onClicked: {
            pageStack.layers.push(Qt.resolvedUrl("ProfilePage.qml"), {core: core, currProfile: currProfile})
          }
        }
      }
      QQC2.Label {
        text: i18n("Serial Port:")
        horizontalAlignment: Text.AlignLeft
      }
      QQC2.ComboBox{
        id: serialPort
        model: core.serialPorts
      }
      QQC2.Label {
        text: i18n("Baud Rate:")
        horizontalAlignment: Text.AlignLeft
      }
      QQC2.ComboBox{
        id: baudRate
        model: core.portSpeeds
        currentIndex: baudRate.find(currProfile ? MachineInfo.readKey(currProfile, MachineInfo.KEY.BAUDRATE) : 0)
      }
      QQC2.Label {
        text: i18n("Firmware:")
        horizontalAlignment: Text.AlignLeft
      }
      QQC2.ComboBox{
        id: firmware
        model: core.availableFirmwarePlugins
        currentIndex: firmware.find(currProfile ? MachineInfo.readKey(currProfile, MachineInfo.KEY.FIRMWARE) : 0)
      }
    }

    RowLayout{
      Layout.alignment: Qt.AlignHCenter
      QQC2.Button{
          id: newProfile
          text: i18n("New Profile")
          icon.name: "document-new"
          onClicked: {
            pageStack.layers.push(Qt.resolvedUrl("ProfilePage.qml"), {core: core})
          }
      }
      QQC2.Button {
        text: i18n("Connect!")
        icon.name: "network-connect"
        Layout.alignment: Qt.AlignHCenter
        onClicked: {
            pageStack.replace(Qt.resolvedUrl("ManagePrinterPage.qml"),
            { profile: printerProfile.currentText,
              serialPort: serialPort.currentText,
              baudRate: baudRate.currentText,
              firmware: firmware.currentText,
              newConn: true,
              currProfile: currProfile
            })
        }
      }
    }
  }
  Component.onCompleted: core.setSerialTimerInterval(100)
}
