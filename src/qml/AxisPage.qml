/**
 * Copyright (C) 2019 Lays Rodrigues <lays.rodrigues@kde.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import "components" as Components
import QtQuick 2.9
import QtQuick.Controls 2.5 as QQC2
import QtQuick.Layouts 1.1
import org.kde.atcore 1.0
import org.kde.kirigami 2.5 as Kirigami

Kirigami.Page{
  property AtCore core
  ColumnLayout{
    anchors.fill: parent
    RowLayout{
      QQC2.Label{
        text: i18n("Maintenance & Machine Control")
        Layout.fillWidth: true
        horizontalAlignment: Text.AlignHCenter
      }
    }
    RowLayout{
      spacing: 16
      Layout.fillHeight: true
      ColumnLayout{
        QQC2.Button{
          flat: true
          icon.name: "go-home"
          icon.width: 32
          icon.height: 32
          text: "All"
          Layout.fillHeight: true
          onClicked: {
            core.home()
          }
        }
        QQC2.Button{
          flat: true
          icon.name: "go-home"
          icon.width: 32
          icon.height: 32
          text: "X"
          Layout.fillHeight: true
          onClicked: core.home(AtCore.X)
        }
        QQC2.Button{
          flat: true
          icon.name: "go-home"
          icon.width: 32
          icon.height: 32
          text: "Y"
          Layout.fillHeight: true
          onClicked: core.home(AtCore.Y)
        }
        QQC2.Button{
          flat: true
          icon.name: "go-home"
          icon.width: 32
          icon.height: 32
          text: "Z"
          Layout.fillHeight: true
          onClicked: core.home(AtCore.Z)
        }
      }
      RowLayout{
        GridLayout{
          columns: 3
          ColumnLayout{
            id: axisXUp
            Layout.columnSpan: 3
            Layout.alignment: Qt.AlignHCenter
            QQC2.Button{
              flat: true
              icon.name: "go-up-skip"
              Layout.alignment: Qt.AlignHCenter
              icon.width: 32
              icon.height: 32
              onClicked: moveAxis(AtCore.X, 1.0)
              Layout.fillWidth: true
              Layout.fillHeight: true
            }
            QQC2.Button{
              flat: true
              icon.name: "go-up-skip"
              Layout.alignment: Qt.AlignHCenter
              onClicked: moveAxis(AtCore.X, 0.5)
              Layout.fillWidth: true
              Layout.fillHeight: true
            }
          }
          RowLayout{
            id: axisYLeft
            QQC2.Button{
              flat: true
              icon.name: "go-previous-skip"
              Layout.alignment: Qt.AlignHCenter
              icon.width: 32
              icon.height: 32
              onClicked: moveAxis(AtCore.Y, 1.0)
              Layout.fillWidth: true
              Layout.fillHeight: true
            }
            QQC2.Button{
              flat: true
              icon.name: "go-previous-skip"
              Layout.alignment: Qt.AlignHCenter
              onClicked: moveAxis(AtCore.Y, 0.5)
              Layout.fillWidth: true
              Layout.fillHeight: true
            }
          }
          QQC2.Label{
            text: "X/Y"
            Layout.alignment: Qt.AlignHCenter
          }
          RowLayout{
            id: axisYRight
            QQC2.Button{
              flat: true
              icon.name: "go-next-skip"
              Layout.alignment: Qt.AlignHCenter
              onClicked: moveAxis(AtCore.Y, 10.0)
              Layout.fillWidth: true
              Layout.fillHeight: true
            }
            QQC2.Button{
              flat: true
              icon.name: "go-next-skip"
              Layout.alignment: Qt.AlignHCenter
              icon.width: 32
              icon.height: 32
              onClicked: moveAxis(AtCore.Y, 1.0)
              Layout.fillWidth: true
              Layout.fillHeight: true
            }
          }
          ColumnLayout{
            id: axisXDown
            Layout.columnSpan: 3
            Layout.alignment: Qt.AlignHCenter
            QQC2.Button{
              flat: true
              icon.name: "go-down-skip"
              Layout.alignment: Qt.AlignHCenter
              onClicked: moveAxis(AtCore.X, 0.5)
              Layout.fillWidth: true
              Layout.fillHeight: true
            }
            QQC2.Button{
              flat: true
              icon.name: "go-down-skip"
              Layout.alignment: Qt.AlignHCenter
              icon.width: 32
              icon.height: 32
              onClicked: moveAxis(AtCore.X, 1.0)
              Layout.fillWidth: true
              Layout.fillHeight: true
            }
          }
        }
        ColumnLayout{
          id: axisZ
            QQC2.Button{
              flat: true
              icon.name: "go-up-skip"
              Layout.alignment: Qt.AlignHCenter
              icon.width: 32
              icon.height: 32
              onClicked: moveAxis(AtCore.Z, 1.0)
              Layout.fillWidth: true
              Layout.fillHeight: true
            }
            QQC2.Button{
              flat: true
              icon.name: "go-up-skip"
              Layout.alignment: Qt.AlignHCenter
              onClicked: moveAxis(AtCore.Z, 0.5)
              Layout.fillWidth: true
              Layout.fillHeight: true
            }
          QQC2.Label{
            text: "Z"
            Layout.alignment: Qt.AlignHCenter
          }
          QQC2.Button{
            flat: true
            icon.name: "go-down-skip"
            Layout.alignment: Qt.AlignHCenter
            onClicked: moveAxis(AtCore.Z, 0.5)
            Layout.fillWidth: true
            Layout.fillHeight: true
          }
          QQC2.Button{
            flat: true
            icon.name: "go-down-skip"
            Layout.alignment: Qt.AlignHCenter
            icon.width: 32
            icon.height: 32
            onClicked: moveAxis(AtCore.Z, 1.0)
            Layout.fillWidth: true
            Layout.fillHeight: true
          }
        }
        ColumnLayout{
          id: axisE
            QQC2.Button{
              flat: true
              icon.name: "go-up-skip"
              Layout.alignment: Qt.AlignHCenter
              icon.width: 32
              icon.height: 32
              onClicked: moveAxis(AtCore.E, 1.0)
              Layout.fillWidth: true
              Layout.fillHeight: true
            }
            QQC2.Button{
              flat: true
              icon.name: "go-up-skip"
              Layout.alignment: Qt.AlignHCenter
              onClicked: moveAxis(AtCore.E, 0.5)
              Layout.fillWidth: true
              Layout.fillHeight: true
            }
          QQC2.Label{
            text: "E"
            Layout.alignment: Qt.AlignHCenter
          }
          QQC2.Button{
            flat: true
            icon.name: "go-down-skip"
            Layout.alignment: Qt.AlignHCenter
            onClicked: moveAxis(AtCore.E, 0.5)
            Layout.fillWidth: true
            Layout.fillHeight: true
          }
          QQC2.Button{
            flat: true
            icon.name: "go-down-skip"
            Layout.alignment: Qt.AlignHCenter
            icon.width: 32
            icon.height: 32
            onClicked: moveAxis(AtCore.E, 1.0)
            Layout.fillWidth: true
            Layout.fillHeight: true
          }
        }
        ColumnLayout{
          QQC2.Label{
              text: i18n("Fan Level")
          }
          QQC2.SpinBox{
            id: fanBoxInput
            editable: true
            from: 0
            to: 100
            onValueChanged: core.setFanSpeed(value)
          }
          QQC2.Label{
            text: i18n("Printer Speed")
          }
          QQC2.SpinBox{
            id: printerSpeedInput
            editable: true
            from: 0
            to: 100
            onValueChanged: core.setPrinterSpeed(value)
          }
          QQC2.Label{
            text: i18n("Flow Rate")
          }
          QQC2.SpinBox{
            id: flowRateInput
            editable: true
            onValueChanged: core.setFlowRate(value)
          }
        }
      }
    }
  }
  function moveAxis(axis, value) {
    core.move(axis, value)
  }
}
