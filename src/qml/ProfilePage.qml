/**
 * Copyright (C) 2019 Lays Rodrigues <lays.rodrigues@kde.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import QtQuick.Controls 2.5 as QQC2
import QtQuick.Layouts 1.1
import org.kde.atcore 1.0
import org.kde.kirigami 2.5 as Kirigami

Kirigami.Page{
    id: profileRoot
    property AtCore core
    property var currProfile: ""
    title: i18n("Create or edit a profile")
    Component.onCompleted: {
        baudRate.currentIndex = currProfile ? baudRate.find(MachineInfo.readKey(currProfile, MachineInfo.KEY.BAUDRATE)) : -1
        firmware.currentIndex = currProfile ? firmware.find(MachineInfo.readKey(currProfile, MachineInfo.KEY.FIRMWARE)) : -1
        autoTempReport.checked = currProfile ? Boolean(MachineInfo.readKey(currProfile, MachineInfo.KEY.AUTOTEMPREPORT)) : true
        isCartesian.checked = currProfile ? Boolean(MachineInfo.readKey(currProfile, MachineInfo.KEY.ISCARTESIAN)) : true
    }
    ColumnLayout{
        anchors.fill: parent
        id: root
        RowLayout{
            Layout.fillWidth: true
            Layout.fillHeight: true
            spacing: 10
            ColumnLayout{
                id: basicInfo
                QQC2.Label{
                    text: i18n("Name")
                }
                QQC2.TextField{
                    id: printerProfile
                    text: currProfile ? currProfile : ""
                }
                QQC2.Label{
                    text: i18n("Baud Rate")
                }
                QQC2.ComboBox{
                    id: baudRate
                    model: core.portSpeeds
                }
                QQC2.Label{
                    text: i18n("Firmware")
                }
                QQC2.ComboBox{
                    id: firmware
                    model: core.availableFirmwarePlugins
                }
                QQC2.Label{
                    text: i18n("Post Pause")
                }
                QQC2.TextField{
                    id: postPause
                    text: currProfile ? MachineInfo.readKey(currProfile, MachineInfo.KEY.POSTPAUSE) : ""
                }
            }
            ColumnLayout{
                id: temperatureInfo
                QQC2.Label{
                    text: i18n("Max Bed Temp")
                }
                QQC2.SpinBox{
                    id: maxBedTemp
                    from: 0
                    to: 170
                    editable: true
                    value: currProfile ? MachineInfo.readKey(currProfile, MachineInfo.KEY.MAXBEDTEMP) : 0
                }
                QQC2.Label{
                    text: i18n("Max Ext Temp")
                }
                QQC2.SpinBox{
                    id: maxtExtTemp
                    from: 0
                    to: 270
                    editable: true
                    value: currProfile ? MachineInfo.readKey(currProfile, MachineInfo.KEY.MAXEXTTEMP) : 0
                }
                QQC2.CheckBox{
                    id: autoTempReport
                    text: i18n("Auto temp report?")
                }
            }
            ColumnLayout{
                id: bedInfo
                QQC2.Label{
                    text: i18n("X Size (mm)")
                }
                QQC2.SpinBox{
                    id: xSize
                    from: 0
                    to: 5000
                    editable: true
                    value: currProfile ? MachineInfo.readKey(currProfile, MachineInfo.KEY.XMAX) : 0
                }
                QQC2.Label{
                    text: i18n("Y Size (mm)")
                }
                QQC2.SpinBox{
                    id: ySize
                    from: 0
                    to: 5000
                    editable: true
                    value: currProfile ? MachineInfo.readKey(currProfile, MachineInfo.KEY.YMAX) : 0
                }
                QQC2.Label{
                    text: i18n("Z Size (mm)")
                }
                QQC2.SpinBox{
                    id: zSize
                    from: 0
                    to: 5000
                    editable: true
                    value: currProfile ? MachineInfo.readKey(currProfile, MachineInfo.KEY.ZMAX) : 0
                }
                QQC2.CheckBox{
                    id: isCartesian
                    text: i18n("Is cartesian?")
                }
            }
        }
        RowLayout{
            Layout.alignment: Qt.AlignRight
            QQC2.Button{
                text: i18n("Go back")
                icon.name: "go-previous"
                onClicked: pageStack.layers.pop()
            }
            QQC2.Button{
                text: i18n("Save")
                icon.name: "document-save"
                onClicked: {
                    if(currProfile){
                        MachineInfo.storeProfile(createProfileDict(currProfile))
                        showPassiveNotification(i18n("Profile saved!"))
                    }else{
                        if(!MachineInfo.profileNames().includes(profileName)){
                            MachineInfo.storeProfile(createProfileDict(profileName))
                            showPassiveNotification(i18n("New profile saved!"))
                        }else {
                            showPassiveNotification(i18n("Profile name already exist. Please change the name!"))    
                        }
                    }
                }
            }
        }
    }
    function createProfileDict(profileName) {
        var profile = {}
        profile[MachineInfo.keyName(MachineInfo.KEY.NAME)] = profileName
        profile[MachineInfo.keyName(MachineInfo.KEY.FIRMWARE)] = firmware.currentText
        profile[MachineInfo.keyName(MachineInfo.KEY.BAUDRATE)] = baudRate.currentText
        profile[MachineInfo.keyName(MachineInfo.KEY.POSTPAUSE)] = postPause.text
        profile[MachineInfo.keyName(MachineInfo.KEY.ISCARTESIAN)] = isCartesian.checkState
        profile[MachineInfo.keyName(MachineInfo.KEY.XMAX)] = xSize.value
        profile[MachineInfo.keyName(MachineInfo.KEY.YMAX)] = ySize.value
        profile[MachineInfo.keyName(MachineInfo.KEY.ZMAX)] = zSize.value
        profile[MachineInfo.keyName(MachineInfo.KEY.AUTOTEMPREPORT)] = autoTempReport.checkState
        profile[MachineInfo.keyName(MachineInfo.KEY.MAXBEDTEMP)] = maxBedTemp.value
        profile[MachineInfo.keyName(MachineInfo.KEY.MAXEXTTEMP)] = maxtExtTemp.value
        return profile
    }
}
