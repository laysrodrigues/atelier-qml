### Atelier Printer Host

Atelier has the goal to be the best open source printer host to control
3DPrinters from your computer in any platform: Linux, Windows and MacOs.

### License
This project is under GPL V3, for more information read LICENSE.

Atelier depends on [AtCore](https://github.com/KDE/atcore), the API to manage the serial connection between the computer and 3D Printers/CNC.
AtCore is also on development by team Atelier.


#### WIP
This current interface is a work in progress by Atelier team to fulfill the need
of a mobile/embedded interface for Atelier.
